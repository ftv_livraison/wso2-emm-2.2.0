function fill(a){
    for(var k in a){
        $('[name="'+k+'"]').val(a[k]);
    }
}

function click(){
        $('[class="btn-download-agent"]').click();
}

function queryParameters () {
	var result = {};

	var params = window.location.search.split(/\?|\&/);

	params.forEach( function(it) {
	    if (it) {
		var param = it.split("=");
		result[param[0]] = param[1];
	    }
	});

	return result;
}

array_example = queryParameters();
fill(array_example);

if(array_example.username)
{
        Cookies.set('migration', '1');
	click();
}
